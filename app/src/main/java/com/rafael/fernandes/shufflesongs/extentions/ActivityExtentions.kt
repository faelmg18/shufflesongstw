package com.rafael.fernandes.shufflesongs.extentions

import android.app.Activity
import android.widget.Toast
import com.rafael.fernandes.shufflesongs.R

fun Activity.showToast(message: String?, time: Int = Toast.LENGTH_LONG) {
    Toast.makeText(
        this,
        if (!message.isNullOrEmpty()) message else this.getString(R.string.generic_erro_message),
        time
    ).show()
}