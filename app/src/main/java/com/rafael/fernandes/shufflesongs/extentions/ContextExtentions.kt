package com.rafael.fernandes.shufflesongs.extentions

import android.content.Context
import androidx.core.content.ContextCompat

fun Context.getColorFromResource(resourceId: Int) =
    ContextCompat.getColor(this, resourceId)