package com.rafael.fernandes.shufflesongs.extentions

import com.rafael.fernandes.domain.model.Songs
import java.util.*
import kotlin.collections.ArrayList

fun ArrayList<Songs>.shuffle(): ArrayList<Songs> {

    var songs = this

    //lista embaralhada
    val shuffledList = ArrayList<Songs>()

    //ir removendo da atual lista
    while (songs.any()) {

        var songElement = getRandomElement(songs)

        //se já tiver gente na lista
        //Se o último da lista for igual ao aleatório gerado
        //e houver pelo menos mais um artista diferente na lista
        //continuo pegando valores aleatórios

        if (shuffledList.any() && shuffledList.last().artistName == songElement.artistName) {

            var differentCurrentSong =
                songs.filter { s -> s.artistName != shuffledList.last().artistName }

            //se houver mais artistas diferentes na lista eu continuo o processo de shuffle
            // pegando apenas os diferentes,
            //garantindo melhor processamento e que o artista não será repetido

            if (differentCurrentSong != null && differentCurrentSong.any()) {
                var newDifferentSong = getRandomElement(differentCurrentSong)
                shuffledList.add(newDifferentSong)
                songs.remove(newDifferentSong)
            } else {
                //caso contrário removo da lista original, pois conforme regra não quero repetições
                songs.remove(songElement)
                shuffledList.add(songElement)
            }

        } else {
            //adiciona na lista shuffle e removo da antiga lista de songs
            shuffledList.add(songElement)
            songs.remove(songElement)
        }
    }

    return shuffledList
}

private fun getRandomElement(songs: List<Songs>): Songs {
    return songs[getIndex(songs.size)]
}

private fun getIndex(maxSize: Int): Int {
    val random = Random()
    return random.nextInt(maxSize)
}