package com.rafael.fernandes.shufflesongs.injection.modules

import com.rafael.fernandes.shufflesongs.ui.screen.activities.main.MainActivity
import com.rafael.fernandes.shufflesongs.ui.screen.activities.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindSplashActivity(): SplashActivity

}