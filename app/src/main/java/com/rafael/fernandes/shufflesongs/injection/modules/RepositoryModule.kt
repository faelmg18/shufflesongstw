package com.rafael.fernandes.shufflesongs.injection.modules

import com.rafael.fernandes.data.remote.repository.SongsImpl
import com.rafael.fernandes.domain.repository.Songs
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    internal fun provideSongsRepository(repositories: SongsImpl): Songs {
        return repositories
    }

}