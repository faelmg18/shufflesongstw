package com.rafael.fernandes.shufflesongs.injection.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rafael.fernandes.shufflesongs.injection.ViewModelKey
import com.rafael.fernandes.shufflesongs.ui.common.ViewModelProviderFactoryJ
import com.rafael.fernandes.shufflesongs.ui.screen.activities.main.MainViewModel
import com.rafael.fernandes.shufflesongs.ui.screen.activities.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelProviderFactoryJ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}