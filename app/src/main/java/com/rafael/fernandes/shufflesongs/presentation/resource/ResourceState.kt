package com.rafael.fernandes.shufflesongs.presentation.resource


sealed class ResourceState {
    object INITIAL : ResourceState()
    object LOADING : ResourceState()
    object SUCCESS : ResourceState()
    object ERROR : ResourceState()
}