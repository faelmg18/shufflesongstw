package com.rafael.fernandes.shufflesongs.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rafael.fernandes.domain.model.Songs
import com.rafael.fernandes.shufflesongs.R
import com.rafael.fernandes.shufflesongs.databinding.SongsItemBinding

class SongsAdapter constructor(private val itemClick: (Songs) -> Unit) :
    ListAdapter<Songs, SongsViewHolder>(SongsDiffCallback()) {
    var imageViewItemClick: ((Songs) -> Unit)? = null
    private var listItem: ArrayList<Songs> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder =
        SongsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.songs_item, parent, false)
        )

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        val binding = holder.binding
        val item = getItem(position);
        binding?.songs = item
        binding?.imageViewAlbumPhoto?.setOnClickListener {
            imageViewItemClick?.invoke(item)
        }
        binding?.executePendingBindings()
        binding?.root?.setOnClickListener {
            itemClick.invoke(item)
        }
    }

    fun getList(): ArrayList<Songs>? {
        return listItem
    }

    override fun submitList(list: MutableList<Songs>?) {
        list?.let {
            this.listItem.clear()
            this.listItem.addAll(it.toList())
        }

        super.submitList(list)
    }

    private class SongsDiffCallback : DiffUtil.ItemCallback<Songs>() {
        override fun areItemsTheSame(oldItem: Songs, newItem: Songs): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Songs, newItem: Songs): Boolean =
            oldItem == newItem
    }
}

class SongsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: SongsItemBinding? = DataBindingUtil.bind(view)
}