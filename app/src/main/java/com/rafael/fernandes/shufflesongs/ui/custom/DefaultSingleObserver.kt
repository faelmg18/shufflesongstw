package com.rafael.fernandes.shufflesongs.ui.custom

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import com.rafael.fernandes.shufflesongs.extentions.setError
import com.rafael.fernandes.shufflesongs.extentions.setLoading
import com.rafael.fernandes.shufflesongs.extentions.setSuccess
import com.rafael.fernandes.shufflesongs.presentation.resource.Resource
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

open class DefaultSingleObserver<T>(@VisibleForTesting val liveDate: MutableLiveData<Resource<T>>) :
    SingleObserver<T> {

    override fun onSubscribe(d: Disposable) {
        liveDate.setLoading()
    }

    override fun onSuccess(value: T) {
        liveDate.setSuccess(value)
    }

    override fun onError(e: Throwable) {
        liveDate.setError(e.message)
    }
}