package com.rafael.fernandes.shufflesongs.ui.screen.activities.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.domain.model.Songs
import com.rafael.fernandes.shufflesongs.R
import com.rafael.fernandes.shufflesongs.databinding.ActivityMainBinding
import com.rafael.fernandes.shufflesongs.extentions.showToast
import com.rafael.fernandes.shufflesongs.extentions.shuffle
import com.rafael.fernandes.shufflesongs.ui.adapters.SongsAdapter
import com.rafael.fernandes.shufflesongs.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private var adapter: SongsAdapter? = null

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {
        setupRecyclerView()
        bindObservables()

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_LIT)) {
            val songsList = savedInstanceState.getSerializable(KEY_LIT) as ArrayList<Songs>
            updateListShuffle(songsList, true)
            return
        } else {
            mViewModel.getSongs()
        }
    }

    private fun bindObservables() {
        mViewModel.mObservableSongs.observe(this, Observer {
            onStateChange(it)
        })
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(this).inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            R.id.shuffle_list -> {
                updateListShuffle(adapter!!.getList()?.shuffle(), true)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun updateListShuffle(list: ArrayList<Songs>?, refresh: Boolean = false) {

        if (adapter == null || refresh) {
            adapter = SongsAdapter {

            }
        }

        recyclerView.adapter = adapter

        val shuffleList = list?.shuffle()
        adapter!!.submitList(shuffleList)
    }

    override fun <H> onSuccess(data: H?) {
        progressBar.visibility = View.GONE
        val item = data as SongContainer
        val songsList = item.results
        if (songsList.size > 0) {
            updateListShuffle(songsList)
        }
    }

    override fun onError(message: String?) {
        progressBar.visibility = View.GONE
        showToast(message)
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter?.let {
            it.getList()?.let { songs ->
                if (songs.isNotEmpty()) {
                    outState.putSerializable(KEY_LIT, adapter!!.getList())
                }
            }
        }
    }

    companion object {
        private val KEY_LIT = "list"
    }
}