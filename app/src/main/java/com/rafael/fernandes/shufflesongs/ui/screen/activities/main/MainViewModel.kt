package com.rafael.fernandes.shufflesongs.ui.screen.activities.main

import androidx.lifecycle.MutableLiveData
import com.rafael.fernandes.domain.interector.GetSongs
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.shufflesongs.presentation.resource.Resource
import com.rafael.fernandes.shufflesongs.ui.base.BaseViewModel
import com.rafael.fernandes.shufflesongs.ui.custom.DefaultSingleObserver
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getSongs: GetSongs
) : BaseViewModel() {

    val mObservableSongs = MutableLiveData<Resource<SongContainer>>()


    fun getSongs() {
        getSongs.executeSingle(
            object : DefaultSingleObserver<SongContainer>(mObservableSongs) {},
            null
        )
    }

    override fun myOnCleared() {
        getSongs.dispose()
    }
}