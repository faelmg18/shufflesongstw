package com.rafael.fernandes.shufflesongs.ui.screen.activities.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.rafael.fernandes.shufflesongs.R
import com.rafael.fernandes.shufflesongs.databinding.SplashActivityBinding
import com.rafael.fernandes.shufflesongs.ui.base.BaseActivity
import com.rafael.fernandes.shufflesongs.ui.screen.activities.main.MainActivity

class SplashActivity : BaseActivity<SplashActivityBinding, SplashViewModel>() {
    override fun layoutId(): Int {
        return R.layout.splash_activity
    }

    override fun beforeSetContentView() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    }

    override fun myOnCreate(savedInstanceState: Bundle?) {
        mViewModel.getLiveData().observe(this, Observer {
            gotoNextScreen(MainActivity::class.java)
        })
    }
}