package com.rafael.fernandes.shufflesongs.ui.screen.activities.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rafael.fernandes.shufflesongs.ui.base.BaseViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashViewModel @Inject constructor() : BaseViewModel() {

    private val mutableLiveData = MutableLiveData<Boolean>()

    fun getLiveData(): LiveData<Boolean> {
        return mutableLiveData
    }

    init {
        GlobalScope.launch {
            delay(TIME_SPLASH)
            mutableLiveData.postValue(true)
        }
    }

    override fun myOnCleared() {

    }

    companion object {
        const val TIME_SPLASH: Long = 5000// 5 segundos
    }
}