package com.rafael.fernandes.shufflesongs.songs_main_viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.rafael.fernandes.domain.interector.GetSongs
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.shufflesongs.helpers.MockHelper
import com.rafael.fernandes.shufflesongs.presentation.resource.Resource
import com.rafael.fernandes.shufflesongs.ui.custom.DefaultSingleObserver
import com.rafael.fernandes.shufflesongs.ui.screen.activities.main.MainViewModel
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

// Pessoal, gostaria de dizer que sou iniciante nos teste, não tive muita vivencia dentro dos projetos
// que atuei devido ao cliente.

class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var mainViewModeli: MainViewModel

    @RelaxedMockK
    lateinit var getSongs: GetSongs

    @Before
    fun before() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> Schedulers.trampoline() }

        mainViewModeli = MainViewModel(
            getSongs
        )
    }

    @Test
    fun `when call getSongs() should change LiveData with new data response`() {
        val dataResponse = MockHelper.songContainerMockResponse
        val mockObserverGetSongs = mockk<Observer<Resource<SongContainer>>>(relaxed = true)

        // Cria um slot para fazer a captura do LiveData
        val slot = slot<DefaultSingleObserver<SongContainer>>()
        every {
            // na chamada do método executeSingle é chamado o capture para fazer a captura do nosso slot do LiveData
            getSongs.executeSingle(capture(slot), any())
        } answers {
            // o arg 0 indica o observable do tipo DefaultSingleObserver
            (arg(0) as DefaultSingleObserver<SongContainer>).onSuccess(dataResponse)
        }

        // criamos nosso observe para capturar quando for passado um valor para ele
        mainViewModeli.mObservableSongs.observeForever(mockObserverGetSongs)
        mainViewModeli.getSongs()

        //asserts
        mainViewModeli.apply {
            assertNotNull(slot.captured.liveDate, "LiveDate is null")
            assertNotNull(slot.captured.liveDate.value, "value of LiveDate is null")
            assertNotNull(slot.captured.liveDate.value!!.data, "Object of LiveData is null")
            assertEquals(
                slot.captured.liveDate.value!!.data!!.resultCount,
                dataResponse.resultCount,
                "Result count is different"
            )
        }

        // Verifica se o onChanged do observable foi chamado ao menos uma vez
        verify(exactly = 1) {
            mockObserverGetSongs.onChanged(any())
        }
    }
}