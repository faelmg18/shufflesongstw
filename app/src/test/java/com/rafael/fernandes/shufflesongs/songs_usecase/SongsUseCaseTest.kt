package com.rafael.fernandes.shufflesongs.songs_usecase

import com.rafael.fernandes.data.network.RestApi
import com.rafael.fernandes.data.remote.repository.SongsImpl
import com.rafael.fernandes.domain.interector.GetSongs
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.domain.repository.Songs
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class SongsUseCaseTest {

    lateinit var songs: Songs
    lateinit var getSongs: GetSongs

    @MockK
    private lateinit var restApi: RestApi

    @Before
    fun before() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        songs = SongsImpl(restApi)

        getSongs = GetSongs(
            mockk(relaxed = true),
            mockk(relaxed = true),
            songs
        )
    }

    @Test
    fun `when call Songs getSongs() should return object sucess`() {

        var responseExpected = SongContainer(0)
        val observable = Single.just(responseExpected)

        every { restApi.getSong() } answers { observable }

        var response = getSongs.buildUseCaseSingle(null)
            .test()
            .assertComplete()
            .assertValue(responseExpected)

        response.dispose()
    }

    @Test
    fun `when call Songs getSongs() should return object fail`() {
        val observable = Single.error<SongContainer>(RuntimeException())

        every { restApi.getSong() } answers { observable }

        val response = getSongs.buildUseCaseSingle(null)
            .test()
            .assertFailure(RuntimeException::class.java)

        response.dispose()
    }
}