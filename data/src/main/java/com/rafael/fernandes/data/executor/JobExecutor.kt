package com.rafael.fernandes.data.executor

import com.rafael.fernandes.domain.executor.ThreadExecutor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class JobExecutor @Inject constructor() : ThreadExecutor {
    private var threadPoolExecutor: ThreadPoolExecutor

    init {
        threadPoolExecutor = ThreadPoolExecutor(
            3, // coreThreadPoolSize,
            5, // maximumThreadPoolSize,
            10L, // Timeout
            TimeUnit.SECONDS,
            LinkedBlockingQueue(), JobThreadFactory()
        )
    }

    override fun execute(command: Runnable) {
        this.threadPoolExecutor.execute(command)
    }

    class JobThreadFactory : ThreadFactory {

        private var counter = 0

        override fun newThread(r: Runnable): Thread {
            return Thread(r, "android_" + counter++)
        }
    }
}