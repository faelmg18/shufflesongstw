package com.rafael.fernandes.data.network

import com.rafael.fernandes.domain.model.SongContainer
import io.reactivex.Single
import retrofit2.http.GET

interface RestApi {
    @GET("/lookup?id=909253,1171421960,358714030,1419227,264111789&limit=5")
    fun getSong():Single<SongContainer>
}