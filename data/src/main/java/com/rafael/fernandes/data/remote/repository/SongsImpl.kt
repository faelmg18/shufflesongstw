package com.rafael.fernandes.data.remote.repository

import com.rafael.fernandes.data.network.RestApi
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.domain.repository.Songs
import io.reactivex.Single
import javax.inject.Inject

class SongsImpl @Inject constructor(private val restApi: RestApi) : Songs {

    override fun getSongs(): Single<SongContainer> {
        return restApi.getSong()
    }
}