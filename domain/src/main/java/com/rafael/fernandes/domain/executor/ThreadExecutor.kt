package com.rafael.fernandes.domain.executor

import java.util.concurrent.Executor

interface ThreadExecutor : Executor