package com.rafael.fernandes.domain.interector

import com.rafael.fernandes.domain.PostExecutionThread
import com.rafael.fernandes.domain.executor.ThreadExecutor
import com.rafael.fernandes.domain.model.SongContainer
import com.rafael.fernandes.domain.repository.Songs
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetSongs @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val songs: Songs
) : UseCase<SongContainer, Void?>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseSingle(params: Void?): Single<SongContainer> {
        return songs.getSongs()
    }

    override fun buildUseCaseObservable(params: Void?): Observable<SongContainer> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}