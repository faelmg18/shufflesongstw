package com.rafael.fernandes.domain.model

import com.google.gson.annotations.SerializedName

data class SongContainer(
    @SerializedName("resultCount") val resultCount: Int,
    @SerializedName("results") val results: ArrayList<Songs> = arrayListOf()
)