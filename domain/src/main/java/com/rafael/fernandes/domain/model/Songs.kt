package com.rafael.fernandes.domain.model

import com.google.gson.annotations.SerializedName

data class Songs(
    @SerializedName("artistName") val artistName: String? = null,
    @SerializedName("id") val id: Int = 0,
    @SerializedName("wrapperType") val wrapperType: String? = null,
    @SerializedName("artistType") val artistType: String? = null,
    @SerializedName("primaryGenreName") val primaryGenreName: String? = null,
    @SerializedName("trackTimeMillis") val trackTimeMillis: Int = 0,
    @SerializedName("collectionName") val collectionName: String? = null,
    @SerializedName("trackExplicitness") val trackExplicitness: String? = null,
    @SerializedName("trackCensoredName") val trackCensoredName: String? = null,
    @SerializedName("collectionId") val collectionId: Int = 0,
    @SerializedName("trackName") val trackName: String? = null,
    @SerializedName("country") val country: String? = null,
    @SerializedName("artworkUrl") val artworkUrl: String? = null,
    @SerializedName("releaseDate") val releaseDate: String? = null,
    @SerializedName("artistId") val artistId: Int = 0
)