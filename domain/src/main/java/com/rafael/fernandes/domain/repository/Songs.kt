package com.rafael.fernandes.domain.repository

import com.rafael.fernandes.domain.model.SongContainer
import io.reactivex.Single


interface Songs {
    fun getSongs(): Single<SongContainer>
}